import * as serverless from 'serverless-http'
import * as express from 'express'
import {
  APIGatewayProxyEvent,
  APIGatewayEventRequestContext,
  Context
} from 'aws-lambda'

import { WebClient } from '@slack/web-api'

const token = process.env.SLACK_TOKEN;
const web = new WebClient(token);
const conversationId = 'CMSKMUFLG';

interface CustomRequest extends express.Request {
  context: APIGatewayEventRequestContext
}

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: false }));

app.get('/', (_: CustomRequest, res: express.Response) => {
  return res.json('hello')
})

app.get('/hello', (_: CustomRequest, res: express.Response) => {
  return res.json("hello")
})

app.post('/post', async function (req, res) {
  console.log(req.body.message)

  try {
    const result = await web.chat.postMessage({
      text: req.body.message,
      channel: conversationId,
    });

    res.json({
      status: 200,
      message: `Successfully send message ${result.ts} in conversation ${conversationId}`
    })
  } catch(e) {
    res.json({ message: e.message })
  }
})

app.post('/conversations', async function (req, res) {
  console.log(req.body.name)

  try {
    const result: any = await web.conversations.create({
      name: req.body.name,
      is_private: req.body.is_private,
      token,
    });

    res.json({
      status: 200,
      message: `Successfully channel created`,
      channelId: result.channel.id,
    })
  } catch(e) {
    res.json({ message: e.message })
  }
})

export const handler = serverless(app, {
  request: (req: CustomRequest, event: APIGatewayProxyEvent, _context: Context) => {
    req.context = event.requestContext;
  },
});